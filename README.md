Web Samples
-----------
> This project is a collection of _micro_ web samples that can be used to learn test automation.

# Pre-requisites

- `Node.js`
- `yarn`

# Setup

- Clone the repository
- Navigate to project directory
- Install project dependencies: `yarn install`
- Run the project: `yarn dev`

# GitLab Pages

These samples are deployed to GitLab Pages: [https://qalabs.gitlab.io/web-samples/snippets/](https://qalabs.gitlab.io/web-samples/snippets/)

