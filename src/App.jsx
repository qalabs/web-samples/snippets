import { Route, Routes } from "react-router-dom";
import { Container, Navbar } from "react-bootstrap";

import Welcome from "./components/welcome.jsx";
import FizzBuzz from "./components/fizzbuzz.jsx";
import Tasks from "./components/tasks.jsx";
import Index from "./components/index.jsx";
import BMICalculator from "./components/bmi.jsx";
import Timer from "./components/timer.jsx";
import Consent from "./components/consent.jsx";
import UploadImage from "./components/uploadImage.jsx";
import Download from "./components/download.jsx";
import Contacts from "./components/contacts.jsx";
import Login from "./components/login.jsx";
import ProgressBar from "./components/progress.jsx";
import OrderList from "./components/orderList.jsx";
import Alerts from "./components/alerts.jsx";
import Cookies from "./components/cookies.jsx";
import Dates from "./components/dates.jsx";
import Inputs from "./components/inputs.jsx";

function App() {
  return (
    <>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#">Web Samples</Navbar.Brand>
        </Container>
      </Navbar>

      <Container className="mt-3">
        <Routes>
          <Route path="/" element={<Index />} />
          <Route path="/welcome" element={<Welcome />} />
          <Route path="/inputs" element={<Inputs />} />
          <Route path="/dates" element={<Dates />} />
          <Route path="/alerts" element={<Alerts />} />
          <Route path="/fizzbuzz" element={<FizzBuzz />} />
          <Route path="/bmi" element={<BMICalculator />} />
          <Route path="/tasks" element={<Tasks />} />
          <Route path="/timer" element={<Timer />} />
          <Route path="/progress" element={<ProgressBar />} />
          <Route path="/cookies" element={<Cookies />} />
          <Route path="/consent" element={<Consent />} />
          <Route path="/upload" element={<UploadImage />} />
          <Route path="/download" element={<Download />} />
          <Route path="/contacts" element={<Contacts />} />
          <Route path="/login" element={<Login />} />
          <Route path="/order-list" element={<OrderList />} />
        </Routes>
      </Container>
    </>
  );
}

export default App;
