import { useState } from "react";
import {
  Container,
  Row,
  Col,
  FormControl,
  Button,
  Card,
  Form,
  Alert,
} from "react-bootstrap";

function Dates() {
  const [date, setDate] = useState("");
  const [time, setTime] = useState("");
  const [dateTimeSubmission, setDateTimeSubmission] = useState([]);
  const [error, setError] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!date || !time) {
      setError("Both date and time are required.");
      return;
    }
    setError(""); // Clear any existing error messages
    setDateTimeSubmission([...dateTimeSubmission, { date, time }]);
    setDate("");
    setTime("");
  };

  return (
    <Container>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Select Date and Time</Card.Title>
              {error && <Alert variant="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit}>
                <Form.Group className="mb-3">
                  <Form.Label>Date</Form.Label>
                  <FormControl
                    type="date"
                    value={date}
                    onChange={(e) => setDate(e.target.value)}
                    isInvalid={!!error && !date}
                    data-test-id="date-input"
                  />
                  <FormControl.Feedback type="invalid">
                    Please choose a date.
                  </FormControl.Feedback>
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label>Time</Form.Label>
                  <FormControl
                    type="time"
                    value={time}
                    onChange={(e) => setTime(e.target.value)}
                    isInvalid={!!error && !time}
                    data-test-id="time-input"
                  />
                  <FormControl.Feedback type="invalid">
                    Please choose a time.
                  </FormControl.Feedback>
                </Form.Group>
                <Button variant="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      {dateTimeSubmission.length > 0 && (
        <Row className="justify-content-center mt-3">
          <Col xs={12} md={6}>
            <Card>
              <Card.Body>
                <Card.Title>Submissions</Card.Title>
                <ul>
                  {dateTimeSubmission.map((entry, index) => (
                    <li key={index}>
                      Date: <span data-test-id="date-value">{entry.date}</span>,
                      Time: <span data-test-id="time-value">{entry.time}</span>
                    </li>
                  ))}
                </ul>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      )}
    </Container>
  );
}

export default Dates;
