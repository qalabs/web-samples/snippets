import React, { useEffect, useState } from "react";
import {
  Alert,
  Button,
  Card,
  Col,
  Container,
  Form,
  ListGroup,
  Row,
} from "react-bootstrap";
import { deleteCookie, getCookie, setCookie } from "../support/cookies";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [rememberMe, setRememberMe] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [showError, setShowError] = useState(false);

  useEffect(() => {
    const rememberUser = getCookie("rememberUser") === "true";
    if (rememberUser) {
      setIsLoggedIn(true);
    }
  }, []);

  const handleLogin = (e) => {
    e.preventDefault();
    if (username === "demo" && password === "demo") {
      setIsLoggedIn(true);
      setShowError(false);
      if (rememberMe) {
        setCookie("rememberUser", rememberMe, 7);
      } else {
        setCookie("rememberUser", rememberMe);
      }
    } else {
      setShowError(true);
    }
  };

  const handleLogout = () => {
    deleteCookie("rememberUser");
    setIsLoggedIn(false);
  };

  if (isLoggedIn) {
    return (
      <Container>
        <Row className="justify-content-center mt-5">
          <Col xs={12} md={6}>
            <Card>
              <Card.Body>
                <Card.Title>Logged in!</Card.Title>
                <Card.Text>
                  You are logged in! Your session will be remembered:{" "}
                  <code>{getCookie("rememberUser") ? "yes" : "no"}</code>.
                  Cookie name is: <code>rememberUser</code>
                </Card.Text>
                <Button onClick={handleLogout}>Logout</Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }

  return (
    <Container>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Login</Card.Title>
              <Form onSubmit={handleLogin}>
                {showError && (
                  <Alert variant="danger">Invalid username or password.</Alert>
                )}
                <Form.Group className="mb-3" autoComplete="off">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Check
                    type="checkbox"
                    label="Remember me"
                    checked={rememberMe}
                    onChange={(e) => setRememberMe(e.target.checked)}
                  />
                </Form.Group>

                <Button variant="primary" type="submit">
                  Login
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Login;
