import { useState } from "react";
import {
  Badge,
  Button,
  Card,
  Col,
  Container,
  FormControl,
  InputGroup,
  ListGroup,
  Modal,
  Row,
} from "react-bootstrap";
import useLocalStorage from "../support/useLocalStorage.js";
import { Link } from "react-router-dom";

function Tasks() {
  const [todos, setTodos] = useLocalStorage("todos", []);
  const [newTodo, setNewTodo] = useState("");
  const [showCompleted, setShowCompleted] = useState(true);
  const [editModalShow, setEditModalShow] = useState(false);
  const [currentTodo, setCurrentTodo] = useState({ id: null, text: "" });

  const visibleTodos = showCompleted
    ? todos
    : todos.filter((todo) => !todo.completed);

  const completedCount = todos.filter((todo) => todo.completed).length;
  const toCompleteCount = todos.length - completedCount;

  const handleAddTodo = () => {
    if (!newTodo.trim()) return;
    setTodos([...todos, { id: Date.now(), text: newTodo, completed: false }]);
    setNewTodo("");
  };

  const handleToggleComplete = (todoId) => {
    const updatedTodos = todos.map((todo) =>
      todo.id === todoId ? { ...todo, completed: !todo.completed } : todo,
    );
    setTodos(updatedTodos);
  };

  const handleDeleteCompleted = () => {
    if (completedCount === 0) return;
    if (window.confirm("Are you sure you want to delete all completed tasks?"))
      setTodos(todos.filter((todo) => !todo.completed));
  };

  const handleDeleteAll = () => {
    if (!todos.length) return;
    if (window.confirm("Are you sure you want to delete all tasks?"))
      setTodos([]);
  };

  function handleExportAsCsv() {
    if (!todos.length) return;
    const contacts = JSON.parse(localStorage.getItem("todos") || "[]");
    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += "ID,Name,Completed\n"; // Header row
    contacts.forEach((todo) => {
      const row = [todo.id, todo.text, todo.completed].join(",");
      csvContent += row + "\n";
    });

    const link = document.createElement("a");
    link.setAttribute("href", encodeURI(csvContent));
    link.setAttribute("download", "todos.csv");

    // Append the link to the body, click it, and then remove it
    document.body.appendChild(link); // Required for Firefox
    link.click();
    document.body.removeChild(link);
  }

  const handleShowEditModal = (todo) => {
    setCurrentTodo(todo);
    setEditModalShow(true);
  };

  const handleEditTodo = () => {
    const updatedTodos = todos.map((todo) =>
      todo.id === currentTodo.id ? { ...todo, text: currentTodo.text } : todo,
    );
    setTodos(updatedTodos);
    setEditModalShow(false);
  };

  const handleEditTodoTextChange = (e) => {
    setCurrentTodo({ ...currentTodo, text: e.target.value });
  };

  return (
    <Container>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Tasks List</Card.Title>
              <InputGroup className="mb-3">
                <FormControl
                  placeholder="Add new task"
                  value={newTodo}
                  onChange={(e) => setNewTodo(e.target.value)}
                  onKeyPress={(e) => e.key === "Enter" && handleAddTodo()}
                  data-test-id="new-todo-input"
                />
                <span
                  className="input-group-text"
                  onClick={handleAddTodo}
                  style={{ cursor: "pointer" }}
                  data-test-id="add-todo-button"
                >
                  Add
                </span>
              </InputGroup>
              <div className="mb-3">
                <a
                  href="#toggle-completed"
                  onClick={(e) => {
                    e.preventDefault();
                    setShowCompleted(!showCompleted);
                  }}
                  style={{ marginRight: "10px", cursor: "pointer" }}
                  data-test-id="toggle-completed"
                >
                  {showCompleted ? "Hide Completed" : "Show Completed"}
                </a>
                <a
                  href="#delete-completed"
                  onClick={(e) => {
                    e.preventDefault();
                    handleDeleteCompleted();
                  }}
                  style={{ marginRight: "10px", cursor: "pointer" }}
                  data-test-id="delete-completed"
                >
                  Delete Completed
                </a>
                <a
                  href="#delete-all"
                  onClick={(e) => {
                    e.preventDefault();
                    handleDeleteAll();
                  }}
                  style={{ cursor: "pointer" }}
                  data-test-id="delete-all"
                >
                  Delete All
                </a>
              </div>
              <ListGroup data-test-id="todos">
                {visibleTodos.map((todo) => (
                  <ListGroup.Item
                    key={todo.id}
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                      textDecoration: todo.completed ? "line-through" : "none",
                      cursor: "pointer",
                    }}
                    data-test-id={`todo-${todo.id}`}
                  >
                    <div onClick={() => handleToggleComplete(todo.id)}>
                      {todo.text}
                    </div>
                    <a
                      href="#edit"
                      onClick={(e) => {
                        e.preventDefault();
                        handleShowEditModal(todo);
                      }}
                      style={{ cursor: "pointer", color: "gray" }}
                      data-test-id="edit-todo"
                    >
                      edit
                    </a>
                  </ListGroup.Item>
                ))}
              </ListGroup>
              <div className="mt-3">
                <Badge bg="success" className="me-2">
                  Completed:{" "}
                  <span data-test-id="completed-count">{completedCount}</span>
                </Badge>
                <Badge bg="primary">
                  To Complete:{" "}
                  <span data-test-id="to-complete-count">
                    {toCompleteCount}
                  </span>
                </Badge>
              </div>
              <div className="mt-3">
                <a
                  href="#export-csv"
                  onClick={(e) => {
                    e.preventDefault();
                    handleExportAsCsv();
                  }}
                  style={{ cursor: "pointer" }}
                  data-test-id="export-csv"
                >
                  Export as CSV
                </a>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <Modal show={editModalShow} onHide={() => setEditModalShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Task</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FormControl
            value={currentTodo.text}
            onChange={handleEditTodoTextChange}
            data-test-id="edit-todo-input"
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setEditModalShow(false)}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={handleEditTodo}
            data-test-id="edit-todo-button"
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default Tasks;
