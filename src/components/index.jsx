import { Card, Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

function Index() {
  return (
    <Container className="mt-5">
      <Row className="justify-content-md-center">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Choose a sample</Card.Title>
              <ul>
                <li>
                  <Link to="/welcome" as={Link}>
                    Welcome (Static Page)
                  </Link>
                </li>
                <li>
                  <Link to="/inputs" as={Link}>
                    Inputs
                  </Link>
                </li>
                <li>
                  <Link to="/dates" as={Link}>
                    Dates
                  </Link>
                </li>
                <li>
                  <Link to="/alerts" as={Link}>
                    Alerts
                  </Link>
                </li>
                <li>
                  <Link to="/fizzbuzz" as={Link}>
                    FizzBuzz
                  </Link>
                </li>
                <li>
                  <Link to="/bmi" as={Link}>
                    BMI Calculator
                  </Link>
                </li>

                <li>
                  <Link to="/tasks" as={Link}>
                    Manage Tasks
                  </Link>
                </li>
                <li>
                  <Link to="/timer" as={Link}>
                    Simple Timer
                  </Link>
                </li>
                <li>
                  <Link to="/progress" as={Link}>
                    ProgressBar
                  </Link>
                </li>
                <li>
                  <Link to="/cookies" as={Link}>
                    Cookies
                  </Link>
                </li>
                <li>
                  <Link to="/consent" as={Link}>
                    User Consent
                  </Link>
                </li>
                <li>
                  <Link to="/upload" as={Link}>
                    Upload Image
                  </Link>
                </li>
                <li>
                  <Link to="/download" as={Link}>
                    Download Files
                  </Link>
                </li>
                <li>
                  <Link to="/contacts" as={Link}>
                    Manage Contacts
                  </Link>
                </li>
                <li>
                  <Link to="/login" as={Link}>
                    Login and Logout
                  </Link>
                </li>
                <li>
                  <Link to="/order-list" as={Link}>
                    Order a List
                  </Link>
                </li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Index;
