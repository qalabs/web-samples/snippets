import { useEffect, useState } from "react";
import { Button, Container, Alert, Card, Row, Col } from "react-bootstrap";

function Alerts() {
  const [confirmationResult, setConfirmationResult] = useState("");

  useEffect(() => {
    const handleKeyDown = (event) => {
      if (event.ctrlKey && event.shiftKey) {
        // Check for Ctrl+Shift combination
        switch (
          event.key.toLowerCase() // Convert to lowercase to ensure case insensitivity
        ) {
          case "a": // Ctrl+Shift+A
            event.preventDefault(); // Prevent default action to avoid interfering with native shortcuts
            handleAlertClick();
            break;
          case "c": // Ctrl+Shift+C
            event.preventDefault();
            handleConfirmationClick();
            break;
          case "p": // Ctrl+Shift+P
            event.preventDefault();
            handlePromptClick();
            break;
          default:
            break;
        }
      }
    };

    // Add event listener for keydown
    document.addEventListener("keydown", handleKeyDown);

    // Cleanup event listener
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, []); // Empty array ensures this effect runs only once after initial render

  const handleAlertClick = () => {
    alert("This is an alert!");
    setConfirmationResult("You dismissed the alert!");
  };

  const handleConfirmationClick = () => {
    const result = window.confirm("Are you sure you want to proceed?");
    setConfirmationResult(result ? "You clicked OK!" : "You clicked Cancel!");
  };

  const handlePromptClick = () => {
    const result = window.prompt("What's on your mind?");
    setConfirmationResult(result ? "You entered: " + result : "No input!");
  };

  return (
    <Container className="mt-5">
      <Row className="justify-content-md-center">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Alerts!</Card.Title>
              <div className="my-3">
                <Button variant="primary" onClick={handleAlertClick}>
                  Alert
                </Button>{" "}
                <Button variant="secondary" onClick={handleConfirmationClick}>
                  Confirm
                </Button>{" "}
                <Button variant="danger" onClick={handlePromptClick}>
                  Prompt
                </Button>{" "}
              </div>
              {confirmationResult && (
                <Alert variant="info">{confirmationResult}</Alert>
              )}
            </Card.Body>
            <Card.Footer>
              <small className="text-muted">
                Press <code>Ctrl+Shift+A</code>, <code>Ctrl+Shift+C</code>, or{" "}
                <code>Ctrl+Shift+P</code> to trigger the respective alert,
                confirm, or prompt.
              </small>
            </Card.Footer>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Alerts;
