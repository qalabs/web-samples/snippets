import { useState } from "react";
import {
  Alert,
  Button,
  Card,
  Col,
  Container,
  Form,
  Row,
} from "react-bootstrap";

const BMI_CATEGORIES = {
  UNDERWEIGHT: "Underweight",
  NORMAL_WEIGHT: "Normal weight",
  OVERWEIGHT: "Overweight",
  OBESITY: "Obesity",
};

function BMICalculator() {
  const [weight, setWeight] = useState("");
  const [height, setHeight] = useState("");
  const [validationState, setValidationState] = useState({
    isValid: true,
    message: "",
  });
  const [bmiResult, setBmiResult] = useState("N/A");
  const [bmiMessage, setBmiMessage] = useState("Enter your weight and height");

  const calculateBMI = (weight, height) => {
    const heightInMeters = height / 100;
    return weight / (heightInMeters * heightInMeters);
  };

  function getAlertVariant(bmiCategory) {
    switch (bmiCategory) {
      case BMI_CATEGORIES.UNDERWEIGHT:
      case BMI_CATEGORIES.OBESITY:
        return "danger";
      case BMI_CATEGORIES.OVERWEIGHT:
        return "warning";
      default:
        return "info";
    }
  }

  const determineBMICategory = (bmi) => {
    if (bmi < 18.5) return BMI_CATEGORIES.UNDERWEIGHT;
    if (bmi >= 18.5 && bmi <= 24.9) return BMI_CATEGORIES.NORMAL_WEIGHT;
    if (bmi >= 25 && bmi <= 29.9) return BMI_CATEGORIES.OVERWEIGHT;
    return BMI_CATEGORIES.OBESITY;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (weight <= 0 || height <= 0) {
      setValidationState({
        isValid: false,
        message: "Please enter valid weight and height.",
      });
      return;
    }
    setValidationState({ isValid: true, message: "" });
    const bmi = calculateBMI(weight, height).toFixed(2);
    const bmiCategory = determineBMICategory(bmi);

    setBmiResult(bmiCategory);
    setBmiMessage(`Your BMI is ${bmi}, indicating ${bmiCategory}.`);
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>BMI Calculator</Card.Title>
              <Form noValidate onSubmit={onSubmit}>
                <Form.Group controlId="formWeightInput">
                  <Form.Label>Weight (kg)</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter weight in kg"
                    value={weight}
                    onChange={(e) => setWeight(e.target.value)}
                    required
                    isInvalid={!validationState.isValid}
                    data-test-id="weight-input"
                  />
                </Form.Group>
                <Form.Group controlId="formHeightInput" className="mt-3">
                  <Form.Label>Height (cm)</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter height in cm"
                    value={height}
                    onChange={(e) => setHeight(e.target.value)}
                    required
                    isInvalid={!validationState.isValid}
                    data-test-id="height-input"
                  />
                  <Form.Control.Feedback type="invalid">
                    {validationState.message}
                  </Form.Control.Feedback>
                </Form.Group>

                <div className="d-grid gap-2 mt-3">
                  <Button
                    variant="primary"
                    type="submit"
                    data-test-id="calculate-bmi-button"
                  >
                    Calculate BMI
                  </Button>
                </div>
              </Form>
            </Card.Body>
          </Card>
        </Col>
        <Col xs={12} md={6}>
          <div>
            <h3>BMI Result</h3>
            <Alert
              variant={getAlertVariant(bmiResult)}
              className="mt-3"
              data-test-id="bmi-result"
              data-bmi-category={bmiResult}
            >
              {bmiMessage}
            </Alert>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default BMICalculator;
