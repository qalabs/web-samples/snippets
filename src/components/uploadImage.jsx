import { useState } from "react";
import { Button, Card, Col, Container, Image, Row } from "react-bootstrap";

function UploadImage() {
  const [image, setImage] = useState(localStorage.getItem("image") || null);

  const handleImageUpload = (event) => {
    const file = event.target.files[0];
    if (file && file.type.startsWith("image/")) {
      const reader = new FileReader();
      reader.onload = function (loadEvent) {
        const base64Image = loadEvent.target.result;
        localStorage.setItem("image", base64Image);
        setImage(base64Image);
      };
      reader.readAsDataURL(file);
    }
  };

  function handleImageDelete() {
    localStorage.removeItem("image");
    setImage(null);
  }

  return (
    <Container>
      <Row className="justify-content-md-center">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Uploading Image</Card.Title>
              <Card.Text>
                This is a simple example of how to upload an image and display
                it in a component. Check <code>localStorage</code> to see the
                base64 encoded image.
              </Card.Text>
              <div className="mb-3">
                <Button variant="primary" as="label">
                  Upload Photo
                  <input type="file" hidden onChange={handleImageUpload} />
                </Button>
              </div>
              {image && (
                <>
                  <hr />
                  <Image src={image} fluid />
                  <hr />
                  <Button variant="danger" onClick={handleImageDelete}>
                    Delete Photo
                  </Button>
                </>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default UploadImage;
