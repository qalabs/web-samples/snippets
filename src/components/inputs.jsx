import React, { useState } from "react";
import {
  Container,
  Row,
  Col,
  Form,
  Button,
  Card,
  Alert,
  ListGroup,
} from "react-bootstrap";

function Inputs() {
  const [formData, setFormData] = useState({
    textInput: "",
    textareaInput: "",
    selectInput: "",
    checkboxInput: false,
    numberInput: "",
    passwordInput: "",
    radioButtonInput: "",
  });
  const [submissionData, setSubmissionData] = useState([]);

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: type === "checkbox" ? checked : value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setSubmissionData([...submissionData, formData]);
    handleReset();
  };

  const handleReset = () => {
    setFormData({
      textInput: "",
      textareaInput: "",
      selectInput: "",
      checkboxInput: false,
      numberInput: "",
      passwordInput: "",
      radioButtonInput: "",
    });
  };

  return (
    <Container>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Inputs</Card.Title>
              <Form onSubmit={handleSubmit}>
                <Form.Group className="mb-3">
                  <Form.Label>Text Input</Form.Label>
                  <Form.Control
                    type="text"
                    required
                    minLength={3}
                    maxLength={20}
                    name="textInput"
                    value={formData.textInput}
                    onChange={handleChange}
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Textarea Input</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={3}
                    required
                    name="textareaInput"
                    value={formData.textareaInput}
                    onChange={handleChange}
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Select Input</Form.Label>
                  <Form.Control
                    as="select"
                    required
                    name="selectInput"
                    value={formData.selectInput}
                    onChange={handleChange}
                  >
                    <option value="">Please select an option</option>
                    <option value="option1">Option 1</option>
                    <option value="option2">Option 2</option>
                  </Form.Control>
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Check
                    type="checkbox"
                    label="Checkbox Input"
                    name="checkboxInput"
                    checked={formData.checkboxInput}
                    onChange={handleChange}
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Number Input</Form.Label>
                  <Form.Control
                    type="number"
                    required
                    min={1}
                    max={100}
                    name="numberInput"
                    value={formData.numberInput}
                    onChange={handleChange}
                  />
                </Form.Group>

                <Form.Group className="mb-3">
                  <Form.Label>Password Input</Form.Label>
                  <Form.Control
                    type="password"
                    required
                    minLength={6}
                    name="passwordInput"
                    value={formData.passwordInput}
                    onChange={handleChange}
                  />
                </Form.Group>
                <Form.Group className="mb-3">
                  <Form.Label>Radio Button Input</Form.Label>
                  <Form.Check
                    type="radio"
                    label="Option 1"
                    name="radioButtonInput"
                    value="Option 1"
                    checked={formData.radioButtonInput === "Option 1"}
                    onChange={handleChange}
                  />
                  <Form.Check
                    type="radio"
                    label="Option 2"
                    name="radioButtonInput"
                    value="Option 2"
                    checked={formData.radioButtonInput === "Option 2"}
                    onChange={handleChange}
                  />
                </Form.Group>

                <Button variant="primary" type="submit">
                  Submit
                </Button>
                <Button
                  variant="secondary"
                  type="reset"
                  onClick={handleReset}
                  className="ms-2"
                >
                  Reset
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>

      {submissionData.length > 0 && (
        <Row className="justify-content-center mt-3">
          <Col xs={12} md={6}>
            <Card>
              <Card.Body>
                <Card.Title>Submission Results</Card.Title>
                {submissionData.map((data, index) => (
                  <>
                    <ListGroup key="{index}">
                      <ListGroup.Item>
                        Text: <code>{data.textInput}</code>
                      </ListGroup.Item>
                      <ListGroup.Item>
                        Textarea: <code>{data.textareaInput}</code>
                      </ListGroup.Item>
                      <ListGroup.Item>
                        Select: <code>{data.selectInput}</code>
                      </ListGroup.Item>
                      <ListGroup.Item>
                        Checkbox:{" "}
                        <code>
                          {data.checkboxInput ? "Checked" : "Not Checked"}
                        </code>
                      </ListGroup.Item>
                      <ListGroup.Item>
                        Number: <code>{data.numberInput}</code>
                      </ListGroup.Item>
                      <ListGroup.Item>
                        Password: <code>{data.passwordInput}</code>
                      </ListGroup.Item>
                      <ListGroup.Item>
                        Radio: <code>{data.radioButtonInput}</code>
                      </ListGroup.Item>
                    </ListGroup>
                    <hr />
                  </>
                ))}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      )}
    </Container>
  );
}

export default Inputs;
