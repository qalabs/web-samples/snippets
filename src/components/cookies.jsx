import { useState } from "react";
import { Button, Card, Col, Container, ListGroup, Row } from "react-bootstrap";

function Cookies() {
  const [cookies, setCookies] = useState([]);

  const handleShowCookies = () => {
    const rawCookies = document.cookie;
    if (rawCookies) {
      const cookiesArray = rawCookies.split(";").map((cookie) => {
        const [name, value] = cookie.split("=").map((c) => c.trim());
        return { name, value };
      });
      setCookies(cookiesArray);
    } else {
      setCookies([]);
    }
  };

  return (
    <Container className="mt-5">
      <Row className="justify-content-md-center">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Cookies!</Card.Title>
              <Button onClick={handleShowCookies} className="mt-3">
                Show Cookies
              </Button>
              {cookies.length > 0 && (
                <ListGroup className="mt-3">
                  {cookies.map((cookie, index) => (
                    <ListGroup.Item key={index} data-test-id="cookie">
                      <code>{cookie.name}</code>: <code>{cookie.value}</code>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Cookies;
