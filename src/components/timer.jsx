import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  FormControl,
  Button,
  Alert,
  Card,
} from "react-bootstrap";

function Timer() {
  const [seconds, setSeconds] = useState("");
  const [timeLeft, setTimeLeft] = useState(0); // Time left in milliseconds
  const [timerActive, setTimerActive] = useState(false);
  const [inputInvalid, setInputInvalid] = useState(false);
  const [timeUp, setTimeUp] = useState(false);

  useEffect(() => {
    let interval = null;
    if (timerActive && timeLeft > 0) {
      interval = setInterval(() => {
        setTimeLeft((prevTimeLeft) => prevTimeLeft - 10);
      }, 10);
    } else if (timeLeft <= 0 && timerActive) {
      clearInterval(interval);
      setTimerActive(false);
      setTimeUp(true);
    }
    return () => clearInterval(interval);
  }, [timerActive, timeLeft]);

  const handleStart = () => {
    const numericSeconds = parseFloat(seconds);
    if (isNaN(numericSeconds) || numericSeconds <= 0) {
      setInputInvalid(true);
      return;
    }
    setInputInvalid(false);
    setTimeUp(false);
    setTimeLeft(numericSeconds * 1000); // Convert seconds to milliseconds
    setTimerActive(true);
  };

  const handlePause = () => {
    setTimerActive(false);
  };

  const handleStop = () => {
    setTimerActive(false);
    setTimeLeft(0);
    setTimeUp(false);
  };

  const handleChange = (e) => {
    setSeconds(e.target.value);
    setInputInvalid(false);
    setTimeUp(false);
  };

  const formatTimeLeft = () => {
    const secondsLeft = Math.floor(timeLeft / 1000);
    const millisecondsLeft = timeLeft % 1000;
    return `${secondsLeft}s ${millisecondsLeft}ms`;
  };

  return (
    <Container>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Timer</Card.Title>
              <FormControl
                type="text"
                placeholder="Enter seconds"
                value={seconds}
                onChange={handleChange}
                isInvalid={inputInvalid}
                data-test-id="timer-input"
              />
              <FormControl.Feedback type="invalid">
                Please enter a valid number greater than 0.
              </FormControl.Feedback>
              <div className="d-grid gap-2 mt-3">
                <Button
                  variant="primary"
                  onClick={handleStart}
                  disabled={timerActive}
                  data-test-id="start-button"
                >
                  Start
                </Button>
                <Button
                  variant="secondary"
                  onClick={handlePause}
                  disabled={!timerActive}
                  data-test-id="pause-button"
                >
                  Pause
                </Button>
                <Button
                  variant="danger"
                  onClick={handleStop}
                  data-test-id="stop-button"
                >
                  Stop
                </Button>
              </div>
              <div
                className="mt-3"
                style={{ fontSize: "24px", fontWeight: "bold" }}
                data-test-id="time-left"
              >
                Time left: {formatTimeLeft()}
              </div>
              {timeUp && (
                <Alert variant="success" className="mt-3" data-test-id="alert">
                  Time is up!
                </Alert>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Timer;
