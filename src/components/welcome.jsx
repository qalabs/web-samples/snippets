import { Card, Col, Container, Row, Table } from "react-bootstrap";
import { useState } from "react";

function Welcome() {
  const technologies = [
    {
      name: "Vite",
      link: "https://vitejs.dev/",
      description:
        "A build tool that aims to provide a faster and leaner development experience for modern web projects.",
    },
    {
      name: "React",
      link: "https://reactjs.org/",
      description:
        "A JavaScript library for building user interfaces, allowing the creation of reusable UI components.",
    },
    {
      name: "React-Router",
      link: "https://reactrouter.com/",
      description:
        "A standard library for routing in React, enabling the navigation among views of various components.",
    },
    {
      name: "React-Bootstrap",
      link: "https://react-bootstrap.github.io/",
      description:
        "An integration of Bootstrap with React, providing Bootstrap front-end framework's look-and-feel with React.",
    },
  ];

  const [clicked, setClicked] = useState(false);

  function clickMe() {
    setClicked((clicked) => !clicked);
    console.log("clicked: ", clicked);
  }

  return (
    <Container className="mt-5">
      <Row className="justify-content-md-center">
        <Col xs={12} md={6}>
          <Card id="card">
            <Card.Body>
              <Card.Title onClick={clickMe}>
                About {clicked ? "💥️" : null}
              </Card.Title>
              <Card.Text as="div">
                <p>
                  This application demonstrates a variety of web development
                  concepts, including:
                </p>
                <ul>
                  <li>Form handling in React</li>
                  <li>Utilizing Bootstrap for styling</li>
                </ul>
                <p>
                  The application is built using the following technologies:
                </p>
                <Table
                  striped
                  bordered
                  hover
                  data-test-id="tech-stack-table"
                >
                  <thead>
                    <tr>
                      <th>Tool</th>
                      <th>About</th>
                    </tr>
                  </thead>
                  <tbody>
                    {technologies.map((tech, index) => (
                      <tr
                        key={index}
                        id={"tech-row-" + index}
                        data-test-id={"tech-row-" + index}
                        className="custom-class"
                      >
                        <td>
                          <a
                            href={tech.link}
                            target="_blank"
                            data-test-id={tech.name.toLowerCase() + "-link"}
                            rel="noopener noreferrer"
                          >
                            {tech.name}
                          </a>
                        </td>
                        <td>{tech.description}</td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Welcome;
