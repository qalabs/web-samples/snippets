import { useState } from "react";
import { Card, Col, Container, ListGroup, Row } from "react-bootstrap";

function OrderList() {
  const [items, setItems] = useState([
    { id: 3, text: "Item 3", variant: "primary" },
    { id: 4, text: "Item 4", variant: "success" },
    { id: 2, text: "Item 2", variant: "info" },
    { id: 1, text: "Item 1", variant: "warning" },
  ]);

  const onDragStart = (e, index) => {
    e.dataTransfer.setData("itemIndex", index);
  };

  const onDragOver = (e) => {
    e.preventDefault();
  };

  const onDrop = (e, dropIndex) => {
    const dragIndex = e.dataTransfer.getData("itemIndex");
    const newList = [...items];
    const itemDropped = newList.splice(dragIndex, 1)[0];
    newList.splice(dropIndex, 0, itemDropped);
    setItems(newList);
  };

  return (
    <Container>
      <Row className="justify-content-center mt-5">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Order the list </Card.Title>
              <ListGroup>
                {items.map((item, index) => (
                  <ListGroup.Item
                    key={item.id}
                    draggable
                    onDragStart={(e) => onDragStart(e, index)}
                    onDragOver={onDragOver}
                    onDrop={(e) => onDrop(e, index)}
                    variant={item.variant}
                    style={{ cursor: "grab", marginBottom: "10px" }}
                    data-test-id={`item-${item.id}`}
                  >
                    {item.text}
                  </ListGroup.Item>
                ))}
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default OrderList;
