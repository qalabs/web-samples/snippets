import { useState } from "react";
import {
  ProgressBar,
  Button,
  Modal,
  Form,
  Container,
  Row,
  Col,
  Card,
} from "react-bootstrap";

function Progress() {
  const [progress, setProgress] = useState(0);
  const [seconds, setSeconds] = useState(2);
  const [showModal, setShowModal] = useState(false);
  const [isRunning, setIsRunning] = useState(false);
  let interval;

  const startProgress = () => {
    setIsRunning(true);
    setProgress(0);
    const intervalTime = (seconds * 1000) / 100; // Divide total time into 100 intervals
    interval = setInterval(() => {
      setProgress((prevProgress) => {
        if (prevProgress >= 100) {
          clearInterval(interval);
          setShowModal(true);
          setIsRunning(false);
          return 100;
        }
        return prevProgress + 1;
      });
    }, intervalTime);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  const cancelProgress = () => {
    clearInterval(interval);
    setIsRunning(false);
    setProgress(0);
  };

  const resetProgress = () => {
    clearInterval(interval);
    setIsRunning(false);
    setProgress(0);
    setSeconds(2); // Reset to default value
  };

  return (
    <Container>
      <Row className="justify-content-md-center my-3">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>ProgressBar Demo</Card.Title>
              <Form>
                <Form.Group controlId="progressSeconds" className="mb-3">
                  <Form.Label>Duration (seconds)</Form.Label>
                  <Form.Control
                    type="number"
                    value={seconds}
                    onChange={(e) => setSeconds(e.target.value)}
                    disabled={isRunning}
                  />
                </Form.Group>
                <Button
                  variant="primary"
                  onClick={startProgress}
                  disabled={isRunning}
                  className="me-2"
                  data-test-id="start-button"
                >
                  Start
                </Button>
                <Button
                  variant="danger"
                  onClick={cancelProgress}
                  disabled={!isRunning}
                  className="me-2"
                  data-test-id="cancel-button"
                >
                  Cancel
                </Button>
                <Button
                  variant="secondary"
                  onClick={resetProgress}
                  disabled={isRunning}
                  data-test-id="reset-button"
                >
                  Reset
                </Button>
              </Form>
              <ProgressBar
                now={progress}
                label={`${progress.toFixed(0)}%`}
                className="my-3"
                data-test-id="progress-bar"
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <Modal show={showModal} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Progress Complete</Modal.Title>
        </Modal.Header>
        <Modal.Body>Progress has finished!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleModalClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default Progress;
