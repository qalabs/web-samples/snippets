import { useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Modal,
  Row,
  Toast,
} from "react-bootstrap";

import { deleteCookie, getCookie, setCookie } from "../support/cookies";

function Consent() {
  const [showConsent, setShowConsent] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState("");
  const [toastVariant, setToastVariant] = useState("info"); // 'info' for accept, 'danger' for reject
  const [consentCookieValue, setConsentCookieValue] = useState("");

  useEffect(() => {
    const consentGiven = getCookie("userConsent");
    setConsentCookieValue(consentGiven);
    if (!consentGiven) {
      setShowConsent(true);
    }
  }, []);

  const handleAccept = () => {
    setCookie("userConsent", "true", 365);
    setShowConsent(false);
    setShowToast(true);
    setToastMessage("Thank you for accepting!");
    setToastVariant("info");
    setConsentCookieValue("true");
  };

  const handleReject = () => {
    setShowConsent(false);
    setShowToast(true);
    setToastMessage("Consent not given.");
    setToastVariant("danger");
  };

  function clearConsent() {
    deleteCookie("userConsent");
  }

  function reload() {
    window.location.reload();
  }

  return (
    <>
      <Container className="mt-5">
        <Row className="justify-content-center mt-5">
          <Col xs={12} md={6}>
            <Card>
              <Card.Body>
                <Card.Title>Consent Component</Card.Title>
                <Card.Text>
                  This page demonstrates a cookie consent overlay. The consent
                  status is saved in a cookie named <code>userConsent</code>.
                  Consent given:{" "}
                  <code>{consentCookieValue ? "yes" : "no"}</code>.
                </Card.Text>
                <div className="my-3">
                  <Button
                    variant="primary"
                    onClick={clearConsent}
                    disabled={!consentCookieValue}
                  >
                    Clear Consent
                  </Button>{" "}
                  <Button variant="secondary" onClick={reload}>
                    Reload
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
      <Modal show={showConsent} backdrop="static" keyboard={false}>
        <Modal.Header>
          <Modal.Title>Cookie Consent</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          We use cookies to improve your experience. By your continued use of
          this site you accept such use.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleReject}>
            Reject
          </Button>
          <Button variant="primary" onClick={handleAccept}>
            Accept
          </Button>
        </Modal.Footer>
      </Modal>
      <Toast
        onClose={() => setShowToast(false)}
        show={showToast}
        delay={5000}
        autohide
        bg={toastVariant}
        position="bottom-end"
        style={{ position: "fixed", bottom: 20, right: 20 }}
      >
        <Toast.Body>{toastMessage}</Toast.Body>
      </Toast>
    </>
  );
}

export default Consent;
