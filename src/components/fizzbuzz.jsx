import { useState } from "react";
import {
  Button,
  Form,
  Container,
  Row,
  Col,
  Modal,
  Card,
} from "react-bootstrap";

function FizzBuzz() {
  const [number, setNumber] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [result, setResult] = useState("");
  const [validationState, setValidationState] = useState({
    isValid: true,
    message: "",
  });

  const handleChange = (e) => {
    setNumber(e.target.value);
    // Reset validation state when user starts typing again
    if (!validationState.isValid) {
      setValidationState({ isValid: true, message: "" });
    }
  };

  const validateInput = () => {
    if (number.trim() === "") {
      setValidationState({ isValid: false, message: "Please enter a number." });
      return false;
    }
    return true;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (!validateInput()) {
      return;
    }
    const num = parseInt(number, 10);
    if (num % 3 === 0 && num % 5 === 0) {
      setResult("FizzBuzz");
    } else if (num % 3 === 0) {
      setResult("Fizz");
    } else if (num % 5 === 0) {
      setResult("Buzz");
    } else {
      setResult(num.toString());
    }
    setShowModal(true);
    setNumber("");
  };

  const handleClose = () => setShowModal(false);

  return (
    <Container className="mt-5">
      <Row className="justify-content-md-center">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>FizzBuzz</Card.Title>
              <Form noValidate onSubmit={onSubmit}>
                <Form.Group controlId="formNumberInput">
                  <Form.Label>Enter a number</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Type here"
                    value={number}
                    onChange={handleChange}
                    isInvalid={!validationState.isValid}
                    data-test-id="number-input"
                  />
                  <Form.Control.Feedback type="invalid">
                    {validationState.message}
                  </Form.Control.Feedback>
                </Form.Group>
                <div className="d-grid gap-2 mt-3">
                  <Button
                    variant="primary"
                    type="submit"
                    data-test-id="check-button"
                  >
                    Check
                  </Button>
                </div>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <Modal show={showModal} onHide={handleClose} data-test-id="result-modal">
        <Modal.Header closeButton>
          <Modal.Title>Result</Modal.Title>
        </Modal.Header>
        <Modal.Body data-test-id="result">{result}</Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={handleClose}
            data-test-id="close-modal-button"
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default FizzBuzz;
