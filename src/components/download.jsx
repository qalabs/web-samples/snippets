import { Card, Col, Container, ListGroup, Row } from "react-bootstrap";

function Download() {
  const fileBasePath = ".";
  return (
    <Container>
      <Row className="justify-content-md-center">
        <Col xs={12} md={6}>
          <Card>
            <Card.Body>
              <Card.Title>Download Files</Card.Title>
              <Card.Text>Download the sample files for the analysis.</Card.Text>
              <ListGroup>
                <ListGroup.Item>
                  <a href={`${fileBasePath}/sample.csv`} download="sample.csv">
                    Download Sample CSV
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a href={`${fileBasePath}/sample.pdf`} download="sample.pdf">
                    Download Sample PDF
                  </a>
                </ListGroup.Item>
                <ListGroup.Item>
                  <a
                    href={`${fileBasePath}/sample.docx`}
                    download="sample.docx"
                  >
                    Download Sample DOCX
                  </a>
                </ListGroup.Item>
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Download;
