import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import { viteStaticCopy } from "vite-plugin-static-copy";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    // Copy the static directory to the root of the build to satisfy Gitlab Pages deployment
    viteStaticCopy({
      targets: [
        {
          src: "static/*",
          dest: "",
        },
      ],
    }),
  ],
  base: "./",
});
